<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/index', function () {
    return view('admin.index');
});
Route::get('/index2', function () {
    return view('admin.index2');
});
Route::get('/index3', function () {
    return view('admin.index3');
});

Route::group([
    'namespace' => 'Admin',
    ], function () {


#region skills
Route::get('skills', 'Skill\ListController')->name('skill.list');
Route::get('skill/edit/{skill?}', 'Skill\FormController')->name('skill.edit');
Route::post('skill/save/{skill?}', 'Skill\SaveController')->name('skill.save');
Route::delete('skill/destroy/{skill}', 'Skill\DestroyController')->name('skill.destroy');
#endregion skills

#region courses
Route::get('courses', 'Course\ListController')->name('course.list');
Route::get('course/edit/{course?}', 'Course\FormController')->name('course.edit');
Route::post('course/save/{course?}', 'Course\SaveController')->name('course.save');
Route::delete('course/destroy/{course}', 'Course\DestroyController')->name('course.destroy');
Route::get('course/skill/{course?}', 'Course\Skill\ListController')->name('course.skill.list');
Route::get('course/skill/add/{course?}', 'Course\Skill\AddSkillController')->name('course.skill.save');
Route::post('course/skill/add/{course?}', 'Course\Skill\AddSkillController@save')->name('course.skill.save');
Route::get('course/skill/edit/{course}/{skill}', 'Course\Skill\EditSkillController')->name('course.skill.edit');
Route::post('course/skill/edit/{course}/{skill}', 'Course\Skill\EditSkillController@save')->name('course.skill.edit');
Route::delete('course/skill/destroy/{course}/{skill}', 'Course\Skill\DestroyController')->name('course.skill.destroy');
#endregion courses

});
