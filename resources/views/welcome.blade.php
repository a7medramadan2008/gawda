<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 5px;
            }
            img:hover{
                /* border: 5px solid #555; */
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class=" container">
            <div class="row">
                @if (Route::has('login'))
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </div>
            {{-- <div class="container"> --}}
                <div class="row">
                    <div class="col-5">
                        <a href="http://www.mans.edu.eg/">
                            <img src="{{asset('admin/logo/AdminLTELogo.png')}}">
                        </a>
                    </div>
                    <div class="col-3" ></div>
                    <div class="col-4" >
                        <a href=" {{url('/') }}/index">
                            <button class="btn btn-outline-dark top-right">
                                admin panel
                            </button>
                        </a>
                    </div>
                </div>

                <br><br><br><br>{{-- <br><br>
                <div class="row">
                    <div class="col-5"></div>
                    <div class="col-3" >
                         <img src="{{asset('admin/logo/AdminLTELogo.png')}}">
                    </div>
                    <div class="col-4" ></div>
                </div> --}}
<br><br><br>
                <div class="row">
                    <div class="col-3"></div>
                    <div class="title m-b-md col-8" style="color: #343a40">
                        {{config('app.name')}}<br>
                    </div>
                    <div class="col-1"></div>
                </div>
            {{-- </div> --}}
        </div>
    </body>
</html>
