@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:10px;">
            <div class="card-header">
                <div class="pull-left">
                    <h2>
                        @if($course->exists)
                        Edit {{$course->name }}
                        @else
                        Create course
                        @endif
                    </h2>
                </div>
                <div class="pull-right" style="float:right;">
                    <a class="btn btn-primary" href="{{ route('course.list') }}"> Back</a>
                </div>
            </div>
                <div class="card-body">
            <form action="{{ route('course.save', $course) }}" method="POST">@csrf
                <div class="row">
                    <div class="col-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Code</strong>
                            <input type="text" name="code" class="form-control" value="{{
            old('name') ?? $course->code }}" required>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Name</strong>
                            <input type="text" name="name" class="form-control" value="{{
            old('name') ?? $course->name }}" required>
                        </div>
                    </div>
                    <div class="col-10 col-sm-10 col-md-10">
                        <div class="form-group">
                            <strong>Description</strong>
                            <textarea type="text" name="description" class="form-control" required>
                    {{old('description') ?? $course->description }}</textarea>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
