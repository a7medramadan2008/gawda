@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:30px;">
            <div class="card-header">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>
                            Add Skill to {{$course->name }} Course
                        </h2>
                    </div>
                    <div class="pull-right" style="float:right;">
                        <a class="btn btn-primary" href="{{ route('course.list') }}"> Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-12">
                    <form action="{{ route('course.skill.save', $course) }}" method="POST">@csrf
                        <div class="col-6">
                            <div class="form-group">
                                <strong>Skills</strong>
                                <select class="form-control" name="skill_id" id="countryOpt" required>
                                    @foreach ($skills as $id => $code)
                                    <option value="{{ $id }}">{{ $code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>wight</strong>
                                <input type="text" name="weight" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
