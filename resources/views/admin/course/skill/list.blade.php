@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:10px;">
            <div class="card-header">
                <h2>Skills List of Course {{ $course->name }}<span style="float:right">
                        <a class="btn btn-primary" href="{{ route('course.skill.save', $course) }}" style="float:right">
                            Add Course Skill
                            <span>&#43;</span>
                        </a>
                    </span> </h2>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Code</strong>
                            <div>{{ $course->code }}</div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Name</strong>
                            <div>{{ $course->name }}</div>
                        </div>
                    </div>
                    <div class="col-10 col-sm-10 col-md-10">
                        <div class="form-group">
                            <strong>Description</strong>
                            <div>{{ $course->description }}</div>
                        </div>
                    </div>
                </div>
                @if ($skills->isEmpty())
                <div class="alert alert-dark">There is no skills added yet</div>
                @else
                <div class="table-responsive">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Weight</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($skills as $skill)
                            <tr>
                                <td>{{ $skill->id }}</td>
                                <td>{{ $skill->code }}</td>
                                <td>{{ $skill->description }}</td>
                                <td>{{ $skill->pivot->weight }}</td>
                                <td width="20%" class="text-right">
                                    <a href="{{ route('course.skill.edit', [$course,$skill])
                      }}" class="btn btn-xs btn-primary text-white mr-2">
                                        <span class="fa fa-pencil">Edite</span>
                                    </a>
                                    <form method="POST" class="float-right" action="{{
                      route('course.skill.destroy',[$course , $skill])}}">@method('delete')@csrf
                                        <button type="submit" title="Delete" class="btn btn-xs btn-danger text-white"
                                            onclick="return confirm('Are you sure ?')">
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <p class="mb-0">'showing'{{ min($skills->perPage(),
                        $skills->total()) }},'of' {{ $skills->total() }}</p>
                <nav>
                    {{ $skills->appends(request()->query())->links() }}
                </nav>
            </div> --}}
            @endif
        </div>
    </div>
</div>

@endsection
