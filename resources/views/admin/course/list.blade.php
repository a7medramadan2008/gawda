@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:10px;">
            <div class="card-header">
                <h2>Courses List </h2>
                <span style="float:right">
                    <a class="btn btn-primary" href="{{ route('course.edit') }}" style="float:right">
                        Create course
                        <span>&#43;</span>
                    </a>
                </span>
            </div>
            <div class="card-body">
                @if ($courses->isEmpty())
                <div class="alert alert-dark">There is no courses added yet</div>
                @else
                <div class="table-responsive">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($courses as $course)
                            <tr>
                                <td>{{ $course->id }}</td>
                                <td>{{ $course->code }}</td>
                                <td>{{ $course->name }}</td>
                                <td>{{ $course->description }}</td>
                                <td width="20%" class="text-right">
                                    <a href="{{ route('course.skill.list', $course)
                      }}" class="btn btn-xs btn-success text-white mr-2">
                                        <span class="fa fa-pencil">skills</span>
                                    </a>
                                    <a href="{{ route('course.edit', $course)
                      }}" class="btn btn-xs btn-primary text-white mr-2">
                                        <span class="fa fa-pencil">Edite</span>
                                    </a>
                                    <form method="POST" class="float-right" action="{{
                      route('course.destroy', $course)}}">@method('delete')@csrf
                                        <button type="submit" title="Delete" class="btn btn-xs btn-danger text-white"
                                            onclick="return confirm('Are you sure ?')">
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <p class="mb-0">'showing'{{ min($courses->perPage(),
                        $courses->total()) }},'of' {{ $courses->total() }}</p>
                <nav>
                    {{ $courses->appends(request()->query())->links() }}
                </nav>
            </div> --}}
            @endif
        </div>
    </div>
</div>
</div>

@endsection
