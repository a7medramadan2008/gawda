@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:10px;">
            <div class="card-header">
                <div class="pull-left">
                    <h2>
                        @if($skill->exists)
                        Edit {{$skill->code }}
                        @else
                        Create Skill
                        @endif
                    </h2>
                </div>
                <div class="pull-right" style="float:right;">
                    <a class="btn btn-primary" href="{{ route('skill.list') }}"> Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('skill.save', $skill) }}" method="POST">@csrf
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Code</strong>
                                <input type="text" name="code" class="form-control" value="{{
            old('name') ?? $skill->code }}" required>
                            </div>
                        </div>
                        <div class="col-10 col-sm-10 col-md-10">
                            <div class="form-group">
                                <strong>Description</strong>
                                <textarea type="text" name="description" class="form-control" required>
                    {{old('description') ?? $skill->description }}</textarea>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
