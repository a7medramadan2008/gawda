@extends('admin.layout')
@section('content')

<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card" style="margin-top:10px;">
            <h2 class="card-header">Skills List<span style="float:right">
                    <a class="btn btn-primary" href="{{ route('skill.edit') }}" style="float:right">
                        Create Skill
                        <span>&#43;</span>
                    </a>
                </span> </h2>
            <div class="card-body">
                @if ($skills->isEmpty())
                <div class="alert alert-dark">There is no skills added yet</div>
                @else
                <div class="table-responsive">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($skills as $skill)
                            <tr>
                                <td>{{ $skill->id }}</td>
                                <td>{{ $skill->code }}</td>
                                <td>{{ $skill->description }}</td>
                                <td width="20%" class="text-right">
                                    <a href="{{ route('skill.edit', $skill)
                      }}" class="btn btn-xs btn-primary text-white mr-2">
                                        <span class="fa fa-pencil">Edite</span>
                                    </a>
                                    <form method="POST" class="float-right" action="{{
                      route('skill.destroy', $skill)}}">@method('delete')@csrf
                                        <button type="submit" title="Delete" class="btn btn-xs btn-danger text-white"
                                            onclick="return confirm('Are you sure ?')">
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <p class="mb-0">'showing'{{ min($skills->perPage(),
                        $skills->total()) }},'of' {{ $skills->total() }}</p>
                <nav>
                    {{ $skills->appends(request()->query())->links() }}
                </nav>
            </div> --}}
            @endif
        </div>
    </div>
</div>
</div>

@endsection
