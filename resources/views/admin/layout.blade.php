@include('admin.parts.head')
@include('admin.parts.nav')
@include('admin.parts.sidebar')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            @include('admin.parts.flash')
            @yield('content')
        </div>
    </section>
</div>
    @include('admin.parts.footer')
