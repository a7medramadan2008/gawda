<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

# Quality System

### first you need to be ready with laravel tooles
[Laravel documentation](https://laravel.com/docs/contributions) ,
 and [Composer](https://getcomposer.org) dependency manager

### Clone

Then create a new file on your computer and clone the project using this command:

```sh
git clone https://gitlab.com/a7medramadan2008/gawda.git
```

### Run this command

```sh
composer install
composer run setup
```


### DB

setup your database configuration in `.env` file

### Migartion

Run this Command

```sh
php artisan migrate:fresh --seed
```

### Create your bransh and then enjonig with Quality System
