<?php

namespace App\Http\Controllers\Admin\Course;

use App\Models\Course;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    public function __invoke(Course $course, Request $request)
    {

        $data = $request->validate([
            'code' => 'required|min:3|max:100|unique:courses,code,' . ($course->id ?? 0),
            'description' => 'required|min:3|max:300',
            'name' => 'required|min:3|max:300',
        ]);

        $course->fill($data)->save();

        $message = $course->wasRecentlyCreated ?
            'Course created successfully' :
            'Course updated successfully';

        return redirect()->route('course.list')->with('success', $message);
    }
}
