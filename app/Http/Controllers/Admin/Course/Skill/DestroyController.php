<?php

namespace App\Http\Controllers\Admin\Course\Skill;

use Illuminate\Routing\Controller;
use App\Models\Course;
use App\Models\Skill;

class DestroyController extends Controller
{
    public function __invoke(Course $course,Skill $skill)
    {
        $course->skills()->detach($skill);

        return redirect()->route('course.skill.list',[$course])->with('success','Skill deleted from Course sucessfully');
    }
}
