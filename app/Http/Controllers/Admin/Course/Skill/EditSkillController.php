<?php

namespace App\Http\Controllers\Admin\Course\Skill;

use App\Models\Course;
use App\Models\Skill;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class EditSkillController extends Controller
{
    public function __invoke(Course $course ,Skill $skill)
    {
        $skills = Skill::pluck('code','id');
        $weight = $course->skills()->where('skill_id',$skill->id)->pluck('weight')->first();
        // dd($skills);
        return view('admin.course.skill.edit-skill',compact('course','skills','skill','weight'));
    }


    public function save(Course $course,skill $skill,Request $request)
    {

        $data = $request->validate([
        'skill_id' => 'required|exists:skills,id',
        'weight' => 'required|integer'
        ]);
        $course->skills()->detach($skill);
        $course->skills()->attach($data['skill_id'], [ 'weight' => $data['weight'] ]);

        return redirect()->route('course.skill.list',[$course])->with('success','Skill edit sucessfully');

    }
}
