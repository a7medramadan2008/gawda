<?php

namespace App\Http\Controllers\Admin\Course\Skill;

use App\Models\Course;
use App\Models\Skill;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AddSkillController extends Controller
{
    public function __invoke(Course $course)
    {
        $skills = Skill::pluck('code','id');;

        return view('admin.course.skill.add-skill',compact('course','skills'));
    }


    public function save(Course $course,Request $request)
    {

        $data = $request->validate([
        'skill_id' => 'required|exists:skills,id',
        'weight' => 'required|integer'
        ]);

        $course->skills()->attach($data['skill_id'], [ 'weight' => $data['weight'] ]);

        return redirect()->route('course.skill.list',[$course])->with('success','Skill Added to Course sucessfully');

    }
}
