<?php

namespace App\Http\Controllers\Admin\Course\Skill;

use Illuminate\Routing\Controller;
use App\Models\Course;

class ListController extends Controller
{
    public function __invoke(Course $course)
    {
        $skills = $course->skills;

        return view('admin.course.skill.list', compact('course', 'skills'));
    }
}
