<?php

namespace App\Http\Controllers\Admin\Course;

use Illuminate\Routing\Controller;
use App\Models\Course;

class DestroyController extends Controller
{
    public function __invoke(Course $course)
    {
        $course->delete();

        return redirect()->route('course.list')->with('success', 'course deleted successfully');
    }
}
