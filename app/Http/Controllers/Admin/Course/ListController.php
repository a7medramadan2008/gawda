<?php

namespace App\Http\Controllers\Admin\Course;

use Illuminate\Routing\Controller;
use App\Models\Course;

class ListController extends Controller
{
    public function __invoke()
    {
        $courses = Course::all();

        return view('admin.course.list', compact('courses'));
    }
}
