<?php

namespace App\Http\Controllers\Admin\Course;

use Illuminate\Routing\Controller;

use App\Models\Course;
use App\Models\Skill;

class FormController extends Controller
{
    public function __invoke(Course $course = null)
    {
        $course = $course ?? Course::make();
        $skills = Skill::with('courses')->get();

        return view('admin.course.form', compact('course', 'skills'));
    }
}
