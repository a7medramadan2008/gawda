<?php

namespace App\Http\Controllers\Admin\Skill;

use Illuminate\Routing\Controller;
use App\Models\Skill;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    public function __invoke(Skill $skill, Request $request)
    {

        $data = $request->validate([
            'code' => 'required|min:3|max:100|unique:skills,code,' . ($skill->id ?? 0),
            'description' => 'required|min:3|max:300',
        ]);

        $skill->fill($data)->save();

        $message = $skill->wasRecentlyCreated ?
            'Skill created successfully':
            'Skill updated successfully';

        return redirect()->route('skill.list')->with('success', $message);
    }
}
