<?php

namespace App\Http\Controllers\Admin\Skill;

use Illuminate\Routing\Controller;
use App\Models\Skill;

class DestroyController extends Controller
{
    public function __invoke(Skill $skill)
    {
        $skill->delete();

        return redirect()->route('skill.list')->with('success', 'Skill deleted successfully');
    }
}
