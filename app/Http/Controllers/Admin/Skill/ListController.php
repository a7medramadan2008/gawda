<?php

namespace App\Http\Controllers\Admin\Skill;

use Illuminate\Routing\Controller;
use App\Models\Skill;

class ListController extends Controller
{
    public function __invoke()
    {
        $skills = Skill::all();

        return view('admin.skill.list', compact('skills'));
    }
}
