<?php

namespace App\Http\Controllers\Admin\Skill;

use Illuminate\Routing\Controller;
use App\Models\Skill;

class FormController extends Controller
{
    public function __invoke(Skill $skill = null)
    {
        $skill = $skill ?? Skill::make();

        return view('admin.skill.form', compact('skill'));
    }
}
