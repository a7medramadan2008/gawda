<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['code','description'];

    #region Relations

    public function Courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'course_skill','skill_id')
        ->withPivot('weight');
    }

    #endregion Relations


}
