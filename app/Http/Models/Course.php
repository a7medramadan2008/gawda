<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'code', 'description','weight'];

    #region Relations

    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class, 'course_skill', 'course_id', 'skill_id')
        ->withPivot('weight');
    }

    #endregion Relations


}
